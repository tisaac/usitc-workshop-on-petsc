# About

This jupyterbook repository collects the materials and examples presented to a group of researchers at the [USITC](https://usitc.gov) during the week of April 19, 2021.

The notebooks will be formatted into a website on the [GitLab page](https://tisaac.gitlab.io/usitc-workshop-on-petsc), but they can be downloaded individually and run.
