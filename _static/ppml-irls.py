#!/usr/bin/env python
# coding: utf-8

# # Zero-to-PPML (IRLS) in PETSc

# As an introduction to how PETSc's data structure may differ from what you are familiar with in python, we re going to implemenet the iteratively reweighted least-squares solver for the gravity model using only the lowest level datatypes, vectors and matrices.

# ## Relevant documentation
# 
# - [Documentation of PETSc's `Vec`](https://docs.petsc.org/en/latest/manual/vec/)
# - [Documentation of PETSc's `Mat`](https://docs.petsc.org/en/latest/manual/mat/)

# In[1]:


get_ipython().run_line_magic('pip', 'install gme')
import sys

#old_stdout = sys.__stdout__
#sys.__stdout__ = sys.stdout


# Let's prepare a dataset for Gravity model estimation, but we will perform the estimation in PETSc.

# In[2]:


import pandas as pd
import numpy as np
import gme
from gme.estimate._ppml_estimation_and_diagnostics import _generate_fixed_effects


# In[3]:


testdata = pd.read_csv('https://tisaac.gitlab.io/usitc-workshop-on-petsc/_static/data/itpde_estim_58.csv')


# In[4]:


gme_data = gme.EstimationData(data_frame = testdata,
                              imp_var_name = 'importer',
                              exp_var_name = 'exporter',
                              trade_var_name = 'trade',
                              year_var_name = 'year')
gme_model = gme.EstimationModel(estimation_data = gme_data,
                                lhs_var = 'trade',
                                rhs_var = ['DIST','FTA','LANG','CNTG'],
                                fixed_effects = ['importer', 'exporter'],
                                keep_years = [2013,2014,2015])
df = gme_data.data_frame
specification = gme_model.specification
fixed_effects_df = _generate_fixed_effects(df, specification.fixed_effects, specification.omit_fixed_effect)
y = df[specification.lhs_var]
X = pd.concat([df[specification.rhs_var], fixed_effects_df], axis=1)

# TODO: Solve X.T @ np.exp(X @ B) = X.T @ y


# ## Vectors
# 
# PETSc's main purpose is to implement **solvers on continuous data**, so the main interface is a hierearchy of algebraic objects.
# 
# The most fundamental type that represents solutions is a **vector in $\mathbb{R}^n$ or $\mathbb{C}^n$**.

# This is more abstract than the data representation in a pandas dataframe: there is no meaning tagged to rows / columns.

# ```{margin}
# PETSc has a data managment object [DM] that keeps track of the meaning/context attached to pieces of data, though it's quite different from a dataframe.
# ```
# [DM]: https://www.mcs.anl.gov/petsc/petsc-main/docs/manualpages/DM/DM.html#DM

# The closest python native equivalent is a 1D `numpy.ndarray` with floating point (or complex floating point) `dtype`.
# 
# The `C` object is `Vec`; the python object is `pets4py.PETSc.Vec`.
# 

# The easiest path from a dataframe is `DataFrame -> ndarray -> Vec`.

# In[ ]:





# Did it work? How can we debug?

# In[ ]:





# ## Viewers
# 
# PETSc has a mechanism like `print` for sending information about its objects to streams.
# 
# (Including `stdout`, `stderr`, and file handles.)

# ```{warning}
# We can use it here in this notebook, but it will look like it doesn't do anything.  Jupyter notebooks don't redirect PETSc's `stdout`.  Check the info stream for the notebook!
# ```

# In[ ]:





# ## Evaluate the inverse link function, $\mu = \exp(\cdot)$.

# In[5]:


def eval_inv_link(m_vec, mu_vec):
    """Use PETSc's built in component-wise exp function"""
    pass


# A few C vs. python interface quirks pop-up:
# 
# - PETSc is written in C, a non-garbage-collected language.  As a consequence, it is very explicit about whether to create new outputs or reuse old ones.  Pre-allocated outputs appear in the function signature.
# 
# - Another resource conservation measure: vector transformations that act in-place, rather than taking an output by default.

# ## Non-standard inverse link functions?
# 
# What if we want to use an inverse link function that doesn't have a built-in PETSc function (like logit)?
# 
# PETSc has a pattern of letting users get/restore access to the raw arrays inside the `Vec` object.
# 
# In python, this is converted to a numpy array.

# In[6]:


def eval_inv_link_custom(m_vec, mu_vec):
    """Use numpy's exp function"""
    pass


# In[7]:


# Test them out


# ## Matrices

# We need to convert the exogenous variables $X$ into a matrix $\in \mathbb{R}^{m \times n}$, which is represented in PETSc by `Mat`.

# ```{note}
# PETSc is object-oriented, but unlike other object-oriented linear algebra interfaces (like you would find in C++), PETSc allows _partial implementation_.
# ```
# 
# Not every `Mat` has to do everything that you could do with a matrix represented by a dense array of $m \times n$ numbers.
# 
# In that sense PETSc's approach is a bit like python: you get not-implemented errors if you try to make an object do something that it can't.
# 
# But just about every `Mat` can be applied to a vector of the appropriate shape.  So `Mat` is always at least a linear operator.

# ## How should we represent $X$?  Sparse or dense?

# The pandas DataFrame has `.values`, which can easily be converted to a PETSc dense matrix.

# In[ ]:





# Now let's convert it to sparse. `numpy.ndarray -> scipy.sparse.csr_matrix -> PETSc AIJ`
# 
# PETSc's default representation of sparse matrices is [_compressed sparse row_](https://en.wikipedia.org/wiki/Sparse_matrix#Compressed_sparse_row_(CSR,_CRS_or_Yale_format)) or AIJ format.

# In[8]:



def csr_to_petsc_aij(csr_mat):
    """Convert a scipy csr matrix into a PETSc AIJ matrix"""
    m, n = csr_mat.shape
    nz_per_row = csr_mat.indptr[1:] - csr_mat.indptr[:-1]
    indptr = csr_mat.indptr.copy()
    indices = csr_mat.indices.copy()
    values = csr_mat.data.copy()
    return PETSc.Mat().createAIJWithArrays([m,n], (indptr, indices, values))


# ### Activity
# 
# In the info stream of the notebook, compare the viewer output of `X_mat_dense.view()` and `X_mat_sparse.view()`.

# In[9]:


#X_mat_dense.view()


# In[10]:


#X_mat_aij.view()


# Now we can define the modeled trade of an estimated $\beta$:

# In[11]:


def modeled_trade(inv_link_func, X_mat, beta_vec, m_vec, mu_vec):
    """
    Inputs:
    
    - inv_link_func: evaluates the inverse link
    - X_mat: exogenous variable matrix
    - beta_vec: model parameters
    - m_vec: temporary storage for X @ beta
    
    Outputs:
    
    - mu = exp(m)
    """
    pass


# In[12]:


# TODO: create the solution vector beta, initialize it to zero, and compute mu = exp(X beta) using modeled_trade


# ## Form the weighted least squares normal equations

# In[13]:


def wls_form(X_mat, X_mat_dup, y_vec, mu_vec, b_vec, A_mat=None):
    """
    Form the weighted least squares system
    
    Inputs:
    
    - X_mat
    - X_mat_dup: space to store diag(mu) @ X_mat
    - y_vec: the target
    - mu_vec the weights
    
    Outputs:
    - b_vec: the residual of the least squares system, X_mat.T @ y_vec
    
    Returns:
    
    - A_mat: newly created X_mat.T @ diag(mu) @ X_mat
    """
    pass


# Note that the pattern that let's us reuse `A_mat` if we have already created it, but create it
# in `X_mat.transposeMatMult()` if we have not created it yet.
# 
# This is because sparse matrix-matrix multiplication has a result whose layout can be difficult to predict,
# but good to reuse once it is known.

# In[14]:


# TODO: form an initial residual using log(y + 10.) as the target for an ordinary least squares system


# ## Solve with the normal equations

# In[15]:


def solve(A_mat, b_vec, beta):
    """
    Inputs:
    
    - A_mat: the normal matrix
    - b_mat: the normal residual
    
    Outputs:
    
    - beta: the least squares estimate
    """
    pass


# This is like matlab "backslash", except that we have to be explicit about when a matrix is considered factored or not.

# ## The final steps

# In[16]:


# TODO: use what you have defined in an IRLS iteration

