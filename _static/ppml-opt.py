import numpy as np
from petsc4py import PETSc


class PPML_PETSc(object):
    """
    X^T (exp(X beta) - y) = 0
    """

    def __init__(self, X, y):
        self.X = X
        self.X_dup = X.duplicate()      # We will store diag(mu) X in X_dup
        self.y = y
        self.res = self.y.duplicate()   # diag(mu) X - y

        # Set up the OLS problem for the initial guess
        self.nzy = y.duplicate()        # data for initial guess
        y.copy(self.nzy)
        nzy_array = self.nzy.getArray()

        self.mu = self.y.duplicate()
        self.mu.set(1.)
        mu_array = self.mu.getArray()
        mu_array[nzy_array == 0.] = 0.
        self.mu.setArray(mu_array)

        nzy_array[nzy_array > 0.] = np.log(nzy_array[nzy_array > 0.])
        self.nzy.setArray(nzy_array)
        X.copy(self.X_dup)
        self.X_dup.diagonalScale(L=self.mu)
        if PETSc.Options().getBool("-store_transpose", False):
            self.XT = self.X.duplicate()
            self.X.copy(self.XT)
            self.XT.transpose()
        else:
            self.XT = None

        # This matrix is initial the normal matrix for the OLS
        # problem, but we will reuse it for the Hessian of the minimization problem
        # as well
        if self.XT:
            self.pmat = self.XT.matMult(self.X_dup)
        else:
            self.pmat = self.X.transposeMatMult(self.X_dup)

    def initGuess(self):
        """Initial guess based on solving the OLS problem X beta = log y
        (where y > 0)"""

        # create a linear solver context
        ksp = PETSc.KSP().create()
        # give the solver a prefix for keys in the options database
        ksp.setOptionsPrefix('init_')
        # use the iterative method LSQR to solve the least squares problem
        ksp.setType('lsqr')
        # tell the linear solver context what X and X^T X are
        # (X^T X will be used to either construct a direct solver or
        # precondition the iterative method)
        ksp.setOperators(self.X_dup, self.pmat)
        sol = self.X_dup.createVecRight()
        # modify the default choices above with runtime options
        ksp.setFromOptions()
        # solve the system
        ksp.solve(self.nzy, sol)
        return sol

    def formObjective(self, tao, beta):
        """Compute \\|mu\\|_1 - (X beta)^T y"""
        self.X.mult(beta, self.mu)
        load = self.mu.dot(self.y)
        self.mu.exp()
        energy = self.mu.norm(norm_type=PETSc.NormType.NORM_1)
        return energy - load

    def formGradient(self, tao, beta, F):
        """Compute X^T(mu - y)"""
        self.X.mult(beta, self.mu)
        self.mu.exp()
        self.mu.copy(self.res)
        self.res.axpy(-1., self.y)
        self.X.multTranspose(self.res, F)

    def formObjectiveGradient(self, tao, beta, F):
        """Both at the same time, reusing some computations"""
        obj = self.formObjective(tao, beta)
        self.mu.copy(self.res)
        self.res.axpy(-1., self.y)
        self.X.multTranspose(self.res, F)
        return obj

    def formHessian(self, snes, beta, H, HP):
        """Compute X^T diag(mu) X """
        self.X.mult(beta, self.mu)
        self.mu.exp()
        self.X.copy(self.X_dup)
        self.X_dup.diagonalScale(L=self.mu)
        if self.XT:
            self.XT.matMult(self.X_dup, result=H)
        else:
            self.X.transposeMatMult(self.X_dup, result=H)

    def tao(self):
        tao = PETSc.TAO().create()
        tao.setObjective(self.formObjective)
        tao.setGradient(self.formGradient)
        tao.setObjectiveGradient(self.formObjectiveGradient)
        tao.setHessian(self.formHessian, self.pmat)
        return tao


def main():

    import os
    import urllib.request

    if not os.path.isfile("y_vec"):
        urllib.request.urlretrieve(
            "https://tisaac.gitlab.io/usitc-workshop-on-petsc/_static/data/y_vec",
            "y_vec")

    if not os.path.isfile("X_mat"):
        urllib.request.urlretrieve(
            "https://tisaac.gitlab.io/usitc-workshop-on-petsc/_static/data/X_mat",
            "X_mat")

    y_file = PETSc.Viewer().createBinary("y_vec", mode='r')
    y = PETSc.Vec().load(y_file)
    y_file.destroy()

    X_file = PETSc.Viewer().createBinary("X_mat", mode='r')
    X = PETSc.Mat().load(X_file)
    X_file.destroy()

    ppml = PPML_PETSc(X, y)
    tao = ppml.tao()
    sol = ppml.initGuess()
    tao.setFromOptions()
    tao.solve(sol)


if __name__ == '__main__':
    main()
