{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "05fa1a8e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Configuring an installation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "78bb4c3d",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The drawback of using an [out of the box](./installing.html#out-of-the-box) installation of PETSc is that the library draws much of its utility from the way that it interfaces with [other packages](https://gitlab.com/petsc/petsc/-/tree/main/config/BuildSystem/config/packages).\n",
    "\n",
    "Package maintainers do not have the time to create distributions of PETSc that link against all of its optional packages.\n",
    "\n",
    "This page will show you how to configure your installation of PETSc to link against different packages and to use different resources (such as GPUs)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f11679f3",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The traditional way to install a custom configuration of PETSc is to install [from source](./installing.html#from-source), by passing additional options flags to `./configure`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3cecdefb",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "`./configure --help` will show you the configuration options: there are a lot.  Let's take a look at the options for just one package: [SuperLU](https://portal.nersc.gov/project/sparse/superlu/)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7dbc0da7",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "```bash\n",
    "$ ./configure --help | grep SUPERLU: -A 18\n",
    "SUPERLU:\n",
    "  --with-superlu=<bool>\n",
    "       Indicate if you wish to test for SuperLU  current: 0\n",
    "  --with-superlu-dir=<dir>\n",
    "       Indicate the root directory of the SuperLU installation\n",
    "  --with-superlu-pkg-config=<dir>\n",
    "       Look for SuperLU using pkg-config utility optional directory to look in\n",
    "  --with-superlu-include=<dirs>\n",
    "       Indicate the directory of the SuperLU include files\n",
    "  --with-superlu-lib=<libraries: e.g. [/Users/..../libsuperlu.a,...]>\n",
    "       Indicate the SuperLU libraries\n",
    "  --download-superlu=<no,yes,filename,url>\n",
    "       Download and install SuperLU  current: no\n",
    "  --download-superlu-commit=commitid\n",
    "       The commit id from a git repository to use for the build of SuperLU  current: 0\n",
    "  --download-superlu-shared=<bool>\n",
    "       Install SUPERLU with shared libraries  current: 0\n",
    "  --download-superlu-cmake-arguments=string\n",
    "       Additional CMake arguments for the build of SuperLU  current: 0\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9b6a94e4",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Although `pip3` does not keep the source tree after a package is installed, [environment variables can be used to configure during installation](https://pypi.org/project/petsc/).\n",
    "\n",
    "In particular, all options flags that can be given to PETSc's `./configure` can be used by putting them in the `PETSC_CONFIGURE_OPTIONS`.\n",
    "\n",
    "Even if you have already installed PETSc, you can reinstall with a new configuration."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5777e9e2",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Use a previously installed package\n",
    "\n",
    "Suppose there is an installation of SuiteSparse already on the system.  For example, using the `apt` package `superlu-dev`, the library that PETSc needs to link against one place (`SUPERLU_LIB=/usr/lib/x86_64-linux-gnu/libsuperlu.so`) and the header files that PETSc needs to compile another place (`SUPERLU_INC=/usr/include/superlu`). "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2fcf1d85",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "```bash\n",
    "PETSC_CONFIGURE_OPTIONS=\"\n",
    "--with-superlu\n",
    "--with-superlu-include=$SUPERLU_INC\n",
    "--with-superlu-lib=$SUPERLU_LIB\" \\\n",
    "pip3 install --ignore-installed petsc\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b35493fe",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Download the package just for PETSc\n",
    "\n",
    "If there is a package that you would like PETSc to use that isn't available on your systems, maybe that package is one that PETSc knows how to download and install for itself.  SuperLU is one of many such packages."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2c24104d",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "```bash\n",
    "PETSC_CONFIGURE_OPTIONS=\"\n",
    "--download-superlu\" \\\n",
    "pip3 install --ignore-installed petsc\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4454db5b",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Use single precision"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ec293bfd",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Later in this workshop we will discuss the tradeoffs between [convergence](./convergence.ipynb) and [performance](./perfintro.ipynb).\n",
    "\n",
    "One parameter that controls the balance between the two is the [floating point precision](https://en.wikipedia.org/wiki/Floating-point_arithmetic#Internal_representation/) used to approximate real numbers."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "74f1ae73",
   "metadata": {},
   "source": [
    "PETSc configures to compile with one internal precision, and defaults to using 64-bit floating point numbers (C `double`s), long the _de facto_ standard in scientific computing.\n",
    "\n",
    "Many machine learning models use 32-bit (`single`) or 16-bit (`__fp16`) precision; some geoscience applications benefit from 128-bit (`__float128`).\n",
    "\n",
    "The option flag `--with-precision=single` configures PETSc to use single precision."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "62280033",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Use accelerator-capable packages\n",
    "\n",
    "PETSc links against several packages that use GPUs or other hosted devices to accelerate calculations.\n",
    "\n",
    "Here is an example configuration that installs PETSc with support for several sparse matrix packages on the CPU and on CUDA compute devices.\n",
    "\n",
    "This node has an Nvidia Volta with compute capablity 7.2, hence `VOLTA72`.  It also has Intel MKL kernels installed on the host.  PETSc cannot download and install MKL kernels, so it doesn't download those, but it can detect where they are."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "61eee8ea",
   "metadata": {},
   "source": [
    "```bash\n",
    "PETSC_CONFIGURE_OPTIONS=\"\n",
    "--download-eigen\n",
    "--download-kokkos\n",
    "--download-kokkos-kernels\n",
    "--download-mumps\n",
    "--download-suitesparse\n",
    "--download-suitesparse-disablegpu=0\n",
    "--download-superlu\n",
    "--download-thrust\n",
    "--download-viennacl\n",
    "--download-yaml\n",
    "--with-kokkos-cuda-arch=VOLTA72\n",
    "--with-mkl_cpardiso\n",
    "\" \\\n",
    "pip3 install --ignore-installed petsc\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18d50ed1",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Configure for performance\n",
    "\n",
    "PETSc can install with many runtime checks and debugging tools that are useful for performance, but detrimental to performance.  When you have a configuration that has been tested and are ready to start [measuring performance](./measurmentintro.ipynb), it is a good idea to install a \"fast\" or \"release\" version of PETSc.\n",
    "\n",
    "To do this you should at least add `--with-debugging=0` to your configuration options.\n",
    "\n",
    "The compilers should also be informed that they should create optimized code as well.  As an example, if you are using a GCC compiler, you can add `--COPTFLAGS=\"-O3 -march=native\"`.  Similar compiler flags are available for C++ (`CXXOPTFLAGS`), fortran (`FOPTFLAGS`) and cuda (`CUDAOPTFLAGS`)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4005be6b",
   "metadata": {},
   "source": [
    "## Further reading\n",
    "\n",
    "- [Official installation guide](https://www.mcs.anl.gov/petsc/documentation/installation.html)\n",
    "- [Example configurations](https://gitlab.com/petsc/petsc/-/tree/main/config/examples) used by PETSc in automated testing\n"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
